# CloveX Editor #

CloveX Editor - powerfull game editor/engine for Love2d Framework ([love2d.org](http://love2d.org))
This is main repo for CloveX project.

### Main features in future (goals): ###

* Project managment
* Entity editor
* Properties inspector
* One-click building
* Entity trees and transformations
* Lighweight Love2d-based Engine and Editor API


### How to run and test it ###
1. Install editor-framework from [official repo](https://github.com/cocos-creator/editor-framework#development).
1. Clone this repo INSIDE editor-framework folder
1. Run clovex-editor (from editor-framework folder) `npm start clovex-editor`(or run it with Electron manually)

More details: https://love2d.org/forums/viewtopic.php?f=5&t=82662

NOTE: Always keep editor-framework up-to-date for compability with new CloveX releases.